﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Timer : MonoBehaviour {

    public float roundTime = 99;

    private Text timerDisplay;

    void Awake()
    {
        timerDisplay = GameObject.FindGameObjectWithTag("Timer").GetComponent<Text>();
    }
    
	void Update () {
        
        if (roundTime > 0)
        {
            roundTime -= Time.deltaTime;
        } else
        {
            roundTime = 0;
            GameObject.FindGameObjectWithTag("GameManager").SendMessage("GameOver", SendMessageOptions.DontRequireReceiver);
        }

        timerDisplay.text = roundTime.ToString("F0");
	}
}
