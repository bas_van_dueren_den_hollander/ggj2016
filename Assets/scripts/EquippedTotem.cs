﻿using UnityEngine;
using System.Collections;

public class EquippedTotem : ScriptableObject {
	public GameObject equippedHead;
	public GameObject equippedBody;
	public GameObject equippedLegs;
}
