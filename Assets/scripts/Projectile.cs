﻿using UnityEngine;
using System.Collections;

public class Projectile : MonoBehaviour {

    public string fromPlayer;
	public int damage;

	// Use this for initialization
	void Start () {
	
	}

	void Update () {
	    
	}

    void OnTriggerEnter2D(Collider2D other)
    {
        if(other.name != fromPlayer
            && other.GetComponent<Projectile>() == null)//other isn't a projectile
        {
			other.SendMessage("TakeDamage", damage,  SendMessageOptions.DontRequireReceiver);

            Destroy(this.gameObject);
        }
    }
}
