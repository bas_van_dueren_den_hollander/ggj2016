﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
//using UnityEditor;

public class Health : MonoBehaviour {

    public string player = "player1";
    private Slider healthSlider;
    public int hitPoints;
	private bool isPoisoned = false;
	public GameObject equippedFeet;

    void Awake()
    {
        hitPoints = 100;
        healthSlider = GameObject.FindGameObjectWithTag(player + "HealthSlider").GetComponent<Slider>();
//		if(this.gameObject.tag == "Player1"){
//			equippedFeet = AssetDatabase.LoadAssetAtPath<EquippedTotem>("Assets/Resources/Scriptable Objects/EquippedTotemsP1.asset");
//		} else {
//			equippedFeet = AssetDatabase.LoadAssetAtPath<EquippedTotem>("Assets/Resources/Scriptable Objects/EquippedTotemsP2.asset");
//		}
    }

    void Update()
    {
        healthSlider.value = hitPoints;
    }

    public void TakeDamage(int amount)
    {
		if(equippedFeet != null && equippedFeet.name.Contains("Buffalo")){
			hitPoints -= (int)(amount * 0.8);
		} else {
	        hitPoints -= amount;
		}
        //Flicker
        this.SendMessage("OnHit", 0.5f, SendMessageOptions.DontRequireReceiver);

        if (hitPoints <= 0)
        {
            Death();
        }
    }

    public void Poisoned(int amount){
		if(!isPoisoned){
			StartCoroutine(PoisonDuration(amount, 5));
			isPoisoned = true;
		} else {
			StopCoroutine("PoisonDuration");
			StartCoroutine(PoisonDuration(amount, 5));
		}
	}

    private void Death()
    {
        GameObject.FindGameObjectWithTag("GameManager").SendMessage("GameOver", SendMessageOptions.DontRequireReceiver);
        print(this.name + " died");
    }

	IEnumerator PoisonDuration(int amount, int ticks){
		if(ticks>0){
			hitPoints -= amount;
			if(hitPoints <= 0){
				Death();
			}
			yield return new WaitForSeconds(0.5f);
			StartCoroutine(PoisonDuration(amount,ticks-1));
		} else {
			isPoisoned = false;
		}
	}
}
