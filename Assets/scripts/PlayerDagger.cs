﻿using UnityEngine;
using System.Collections;
//using UnityEditor;

public class PlayerDagger : MonoBehaviour {

    public GameObject otherPlayer;
    public float attackRange = 0.5f;
	private GameObject thisPlayer;

	private bool attackIsOnCooldown;

    private int controlScheme;
	public GameObject equippedBody;

    Animator anim;
    private float attackSpeed;

	// Use this for initialization
	void Start () {
        controlScheme = this.GetComponent<PlayerMovement>().controlScheme;
		thisPlayer = this.gameObject;

        anim = GetComponent<Animator>();
        
	}


	
	// Update is called once per frame
	void Update () {
        string attackKey = "Melee_P" + (controlScheme);

        if(Input.GetButton(attackKey) && !attackIsOnCooldown)
        {
            this.GetComponent<Animator>().SetTrigger("AttackTrigger");

            if (thisPlayer.gameObject.tag == "Player1")
            {
//              equippedBody = AssetDatabase.LoadAssetAtPath<EquippedTotem>("Assets/Resources/Scriptable Objects/EquippedTotemsP1.asset");
                if (equippedBody != null && equippedBody.name.Contains("Snake"))
                {
                    anim.speed = 1;
                }
                else if (equippedBody != null && equippedBody.name.Contains("Buffalo"))
                {
                    anim.speed = 0.5f;
                }
                else if (equippedBody != null && equippedBody.name.Contains("Owl"))
                {
                    anim.speed = 2;
                }
            } else
            {
//                equippedBody = AssetDatabase.LoadAssetAtPath<EquippedTotem>("Assets/Resources/Scriptable Objects/EquippedTotemsP2.asset");
                if (equippedBody != null && equippedBody.name.Contains("Snake"))
                {
                    anim.speed = 1;
                }
                else if (equippedBody != null && equippedBody.name.Contains("Buffalo"))
                {
                    anim.speed = 0.5f;
                }
                else if (equippedBody != null && equippedBody.name.Contains("Owl"))
                {
                    anim.speed = 2;
                }
            }
        }
	}

    private float getPlayerDistance()
    {
        Vector3 tempVect = otherPlayer.transform.position - this.transform.position;

        return tempVect.magnitude;
    }

    private void attack()
    {
        if (getPlayerDistance() < attackRange)
        {
            otherPlayer.SendMessage("TakeDamage", 10, SendMessageOptions.DontRequireReceiver);

            if (thisPlayer.gameObject.tag == "Player1")
            {
                attackIsOnCooldown = true;
//              equippedBody = AssetDatabase.LoadAssetAtPath<EquippedTotem>("Assets/Resources/Scriptable Objects/EquippedTotemsP1.asset");
                if (equippedBody != null && equippedBody.name.Contains("Snake"))
                {
                    otherPlayer.SendMessage("Poisoned", 2, SendMessageOptions.DontRequireReceiver);
                    StartCoroutine("AttackCooldown", 1.5f);

                    //attackSpeed = 1f;
                }

                else if (equippedBody != null && equippedBody.name.Contains("Buffalo"))
                {
                    otherPlayer.SendMessage("TakeDamage", 30, SendMessageOptions.DontRequireReceiver);
                    StartCoroutine("AttackCooldown", 3);

                    //attackSpeed = 0.5f;
                }

                else if (equippedBody != null && equippedBody.name.Contains("Owl"))
                {
                    StartCoroutine("AttackCooldown", 0.75f);
                    //attackSpeed = 0.01f;

                }
                else {
                    StartCoroutine("AttackCooldown", 1.5f);
                    //attackSpeed = 1;
                }
            }
            else {
                attackIsOnCooldown = true;
//              equippedBody = AssetDatabase.LoadAssetAtPath<EquippedTotem>("Assets/Resources/Scriptable Objects/EquippedTotemsP2.asset");
                if (equippedBody != null && equippedBody.name.Contains("Snake"))
                {
                    otherPlayer.SendMessage("Poisoned", 2, SendMessageOptions.DontRequireReceiver);
                    StartCoroutine("AttackCooldown", 1.5f);
                }

                else if (equippedBody != null && equippedBody.name.Contains("Buffalo"))
                {
                    Debug.Log("Buffalo dmg");
                    otherPlayer.SendMessage("TakeDamage", 30, SendMessageOptions.DontRequireReceiver);
                    StartCoroutine("AttackCooldown", 3);
                }

                else if (equippedBody != null && equippedBody.name.Contains("Owl"))
                {
                    StartCoroutine("AttackCooldown", 0.75f);

                }
                else {
                    StartCoroutine("AttackCooldown", 1.5f);
                }
            }
        }
    }

    void stopAttackTrigger ()
    {
        this.GetComponent<Animator>().ResetTrigger("AttackTrigger");
        anim.speed = 1;
    }

	IEnumerator AttackCooldown(float cooldown){
		yield return new WaitForSeconds(cooldown);
		attackIsOnCooldown = false;
	}
}
