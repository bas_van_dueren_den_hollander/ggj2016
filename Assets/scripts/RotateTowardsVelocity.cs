﻿using UnityEngine;
using System.Collections;

public class RotateTowardsVelocity : MonoBehaviour {
	
	// Update is called once per frame
	void Update () {
        Vector3 velocity = this.GetComponent<Rigidbody2D>().velocity;
        this.transform.rotation.SetLookRotation(velocity);
	}
}
