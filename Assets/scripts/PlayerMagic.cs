﻿using UnityEngine;
using System.Collections;
//using UnityEditor;

public class PlayerMagic : MonoBehaviour {

    public GameObject projectile;
    public GameObject owlProjectile;
    public GameObject buffaloProjectile;
    public GameObject snakeProjectile;

    public float projectileSpeed;
    private float fireDelay = 0.8f;

    private int controlScheme;
    private float lastShot = 0;

    Vector2 aimingVect = Vector2.right;

    public GameObject equippedHead;

    // Use this for initialization
    void Start() {
        controlScheme = this.GetComponent<PlayerMovement>().controlScheme;
    }

    // Update is called once per frame
    void Update() {
        string fireButtonName = "Magic_P" + (controlScheme);

        if (Input.GetButton(fireButtonName))
        {
            FireMagic();
        }
    }

    void FireMagic()
    {
        GameObject firedProjectile;

        if (Time.time < fireDelay + lastShot)
        {
            return;
        }

//      equippedTotem = AssetDatabase.LoadAssetAtPath<EquippedTotem>("Assets/Resources/Scriptable Objects/EquippedTotemsP" + controlScheme + ".asset");
        if (equippedHead != null && equippedHead.name.Contains("Owl"))
        {
            owlProjectile.GetComponent<Chasing>().target = GameObject.FindGameObjectWithTag("Player" + (3 - controlScheme));

            firedProjectile = (GameObject)Instantiate(owlProjectile, this.transform.position, this.transform.rotation);

        } else if (equippedHead != null && equippedHead.name.Contains("Buffalo"))
        {
            firedProjectile = (GameObject)Instantiate(buffaloProjectile, this.transform.position, this.transform.rotation);

        } else if (equippedHead != null && equippedHead.name.Contains("Snake"))
        {
            firedProjectile = (GameObject)Instantiate(snakeProjectile, this.transform.position, this.transform.rotation);

        }
            else
        {
            firedProjectile = (GameObject)Instantiate(projectile, this.transform.position, this.transform.rotation);

        }


        firedProjectile.GetComponent<Projectile>().fromPlayer = this.name;

        Vector2 stickInput = Vector2.zero;
        
        stickInput = new Vector2(Input.GetAxis("Horizontal_P" + controlScheme), -Input.GetAxis("Vertical_P" + controlScheme));

        if (stickInput.magnitude > 0.4f)
        {
            aimingVect = stickInput;
            aimingVect.Normalize();
            aimingVect *= projectileSpeed;
        }

        if (equippedHead != null && equippedHead.name.Contains("Snake"))
        {
            //firedProjectile.transform.rotation.SetLookRotation(aimingVect, Vector3.forward);
            float angle = Mathf.Atan2(aimingVect.x - this.transform.position.x, aimingVect.y - this.transform.position.y) * -Mathf.Rad2Deg;
            //Vector2.Angle(aimingVect, Vector2.up);

            firedProjectile.transform.rotation = Quaternion.Euler(0, 0, angle);
        } else
        {
            firedProjectile.GetComponent<Rigidbody2D>().AddForce(aimingVect, ForceMode2D.Impulse);
        }

        lastShot = Time.time;
    }
}