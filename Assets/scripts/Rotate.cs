﻿using UnityEngine;
using System.Collections;

public class Rotate : MonoBehaviour {

    public float speed = 360f;
	
	// Update is called once per frame
	void Update () {
        this.transform.eulerAngles = new Vector3(0, 0, Time.time * speed);
	}
}
