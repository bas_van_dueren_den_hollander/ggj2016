﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GetGUIImage : MonoBehaviour {
   

    public Sprite[] SkillImages;
    public Image HeadImageP1;
    public Image BodyImageP1;
    public Image LegImageP1;
    public Image HeadImageP2;
    public Image BodyImageP2;
    public Image LegImageP2;
    // Use this for initialization

    void Awake()
    {
        SkillImages = Resources.LoadAll<Sprite>("");
        HeadImageP1 = GameObject.FindGameObjectWithTag("P1Head").GetComponent<Image>();
        BodyImageP1 = GameObject.FindGameObjectWithTag("P1Body").GetComponent<Image>();
        LegImageP1 = GameObject.FindGameObjectWithTag("P1Legs").GetComponent<Image>();
        HeadImageP2 = GameObject.FindGameObjectWithTag("P2Head").GetComponent<Image>();
        BodyImageP2 = GameObject.FindGameObjectWithTag("P2Body").GetComponent<Image>();
        LegImageP2 = GameObject.FindGameObjectWithTag("P2Legs").GetComponent<Image>();
    }
	void Start () {
       
    }
	
	// Update is called once per frame
	void Update () {
        
	}


    public void Player1() {
        if (gameObject.tag == "HeadObj")
        {
            for (int i = 0; i < SkillImages.Length; i++)
            {

                HeadImageP1.sprite = SkillImages[i];
              
            }
        }
        else if (gameObject.tag == "BodyObj")
        {
            for (int i = 0; i < SkillImages.Length; i++)
            {

                BodyImageP1.sprite = SkillImages[i];
                
            }
        }

        else if (gameObject.tag == "BodyObj")
        {
            for (int i = 0; i < SkillImages.Length; i++)
            {

                LegImageP1.sprite = SkillImages[i];
               
            }
        }
    }
   public void Player2() {
        if (gameObject.tag == "HeadObj")
        {
            for (int i = 0; i < SkillImages.Length; i++)
            {
                if(gameObject.name == "Head")
               
                HeadImageP2.sprite = SkillImages[i];
            }
        }
        else if (gameObject.tag == "BodyObj")
        {
            for (int i = 0; i < SkillImages.Length; i++)
            {

               
                BodyImageP2.sprite = SkillImages[i];
            }
        }

        else if (gameObject.tag == "BodyObj")
        {
            for (int i = 0; i < SkillImages.Length; i++)
            {

                
                LegImageP2.sprite = SkillImages[i];
            }
        }
    }
}
  




