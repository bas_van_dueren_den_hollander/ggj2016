﻿using UnityEngine;
using System.Collections;

public class FlipTowards : MonoBehaviour {

    public Transform target;
	
	// Update is called once per frame
	void Update () {
        if (target.position.x > this.transform.position.x)
        {
            this.transform.localScale = new Vector3(-1f, 1f, 1f);
        }
        else
        {
            this.transform.localScale = new Vector3(1f, 1f, 1f);
        }
	}
}
