﻿using UnityEngine;
using System.Collections;

public class KnockBack : MonoBehaviour
{

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.name != GetComponent<Projectile>().fromPlayer
            && other.GetComponent<Projectile>() == null//other isn't a projectile
            && other.tag.Contains("Player"))
        {
            other.GetComponent<Rigidbody2D>().AddForce(gameObject.GetComponent<Rigidbody2D>().velocity, ForceMode2D.Impulse);
        }
    }
}
