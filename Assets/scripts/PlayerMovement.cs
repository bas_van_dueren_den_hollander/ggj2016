﻿using UnityEngine;
using System.Collections;
//using UnityEditor;

public class PlayerMovement : MonoBehaviour {
    
    public float playerSpeed = 6f;

    public int controlScheme = 1;

    private float deadZone = 0.4f;

    public float maxHeight = 7f;
    public float maxWidth = 15f;

	public GameObject snakeTail;
	private bool dropSnakeTail = false;

    public float heightOffset;

	public GameObject playerManager;
	public GameObject equippedFeet;

    float horizontalAxis;
    float verticalAxis;


	// Use this for initialization
	void Start () {
		TotemPickup temp = this.gameObject.GetComponent<TotemPickup>();
		equippedFeet = temp.equippedFeet;
//		if(this.gameObject.tag == "Player1"){
//			equippedFeet = AssetDatabase.LoadAssetAtPath<EquippedTotem>("Assets/Resources/Scriptable Objects/EquippedTotemsP1.asset");
//		} else {
//			equippedFeet = AssetDatabase.LoadAssetAtPath<EquippedTotem>("Assets/Resources/Scriptable Objects/EquippedTotemsP2.asset");
//		}
		StartCoroutine("SnakeDelay",0.05f);
	}
	
	// Update is called once per frame
	void Update ()
    {
        this.GetComponent<Animator>().SetBool("IsWalking", true);

        Vector2 stickInput = Vector2.zero;
        if (controlScheme == 1)
        {
            stickInput = new Vector2(Input.GetAxis("Horizontal_P1"), -Input.GetAxis("Vertical_P1"));
        }
        else
        {
            stickInput = new Vector2(Input.GetAxis("Horizontal_P2"), -Input.GetAxis("Vertical_P2"));
        }
        if (stickInput.magnitude < deadZone)
        {
            stickInput = Vector2.zero;
            this.GetComponent<Animator>().SetBool("IsWalking", false);
        }

		if (equippedFeet != null && equippedFeet.name.Contains("Owl")){
			Move( (new Vector3(stickInput.x, stickInput.y))*(playerSpeed + 2));
		}

		else if (equippedFeet != null && equippedFeet.name.Contains("Buffalo")){
			Move( (new Vector3(stickInput.x, stickInput.y))*(playerSpeed - 2));
		}

		else if (equippedFeet != null && equippedFeet.name.Contains("Snake")){
			Move( (new Vector3(stickInput.x, stickInput.y))*(playerSpeed));
			if(dropSnakeTail){
				GameObject trail = (GameObject)Instantiate(snakeTail,this.gameObject.transform.position, Quaternion.identity);
				trail.GetComponent<Projectile>().fromPlayer = this.name;
				dropSnakeTail = false;
			}
		} else {
			Move( (new Vector3(stickInput.x, stickInput.y))*playerSpeed);
		}
			
    }

    void Move (Vector3 movementVector)
    {
        Vector3 perStep = movementVector * Time.deltaTime;

        this.transform.position += perStep;
        
        GetComponent<Renderer>().sortingOrder = -(int)this.transform.position.y;

        ClampPosition();
    }

    void ClampPosition()
    {
        if (this.transform.position.x > maxWidth / 2)
        {
            this.transform.position = new Vector3(maxWidth / 2, this.transform.position.y);
        }
        if (this.transform.position.x < -maxWidth / 2)
        {
            this.transform.position = new Vector3(-maxWidth / 2, this.transform.position.y);
        }
        if (this.transform.position.y > maxHeight / 2 - heightOffset)
        {
            this.transform.position = new Vector3(this.transform.position.x, maxHeight / 2 - heightOffset);
        }
        if (this.transform.position.y < -maxHeight / 2 - heightOffset)
        {
            this.transform.position = new Vector3(this.transform.position.x, -maxHeight / 2 - heightOffset);
        }
    }

	IEnumerator SnakeDelay(float delay){
		yield return new WaitForSeconds(delay);
		dropSnakeTail = true;
		StartCoroutine("SnakeDelay",delay);
	}
}
