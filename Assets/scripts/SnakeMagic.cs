﻿using UnityEngine;
using System.Collections;

public class SnakeMagic : MonoBehaviour {

    public GameObject snakeTail;
    private bool dropSnakeTail = false;
    private float sinusFunction;

    void Start()
    {

        StartCoroutine("SnakeDelay", 0.05f);
    }

    void Update()
    {
        sinusFunction += Time.deltaTime;
        this.gameObject.transform.position += this.gameObject.transform.right * Mathf.Sin(Time.time*25) * 0.3f;

        if (dropSnakeTail)
        {
            GameObject trail = (GameObject)Instantiate(snakeTail, this.gameObject.transform.position, Quaternion.identity);
            trail.GetComponent<Projectile>().fromPlayer = this.gameObject.GetComponent<Projectile>().fromPlayer;
            dropSnakeTail = false;
        }
    }

    IEnumerator SnakeDelay(float delay)
    {
        yield return new WaitForSeconds(delay);
        dropSnakeTail = true;
        StartCoroutine("SnakeDelay", delay);
    }
}
