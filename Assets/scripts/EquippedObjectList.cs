﻿using UnityEngine;
using System.Collections;

public class EquippedObjectList : MonoBehaviour {

	public GameObject equippedHead;
	public GameObject equippedBody;
	public GameObject equippedFeet;
}
