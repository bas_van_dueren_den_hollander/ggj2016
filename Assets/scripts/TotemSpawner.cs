﻿using UnityEngine;
using System.Collections;

public class TotemSpawner : MonoBehaviour {

    public GameObject[] possibleTotems;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void SpawnTotem()
    {
        int totemIndex = (int)Mathf.Floor(Random.Range(0, possibleTotems.Length));

        GameObject totem = possibleTotems[totemIndex];

        Instantiate(totem, this.transform.position, this.transform.rotation);
    }
}
