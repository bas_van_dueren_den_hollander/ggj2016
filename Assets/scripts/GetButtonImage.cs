﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GetButtonImage : MonoBehaviour {
    public GameObject gameManager;
    private GameManager gm;
	// Use this for initialization
	void Awake () {
        gameManager = GameObject.FindGameObjectWithTag("GameManager");
        gm = gameManager.GetComponent<GameManager>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void MaskGet()
    {
        gm.GetMask(this.GetComponent<Image>().sprite);
    }

}
