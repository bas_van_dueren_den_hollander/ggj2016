﻿using UnityEngine;
using System.Collections;

public class TotemSpawnerManager : MonoBehaviour {

    public float averageSpawnDelay = 10f;

    public TotemSpawner[] totemSpawners;

    private float lastSpawnTime = 0;
    private float nextDelay = 0;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	    if(Time.time < lastSpawnTime + nextDelay)
        {
            return;
        }

        int totemIndex = (int)Mathf.Floor(Random.Range(0, totemSpawners.Length));
        totemSpawners[totemIndex].SpawnTotem();
        
        nextDelay = averageSpawnDelay * (Random.value + 0.5f);

        lastSpawnTime = Time.time;
	}
}
