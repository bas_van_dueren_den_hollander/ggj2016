﻿using UnityEngine;
using System.Collections;

public class MaskSpriteGetter : MonoBehaviour {
    
    public int playerNr;

	// Use this for initialization
	void Start () {
        SpriteRenderer sr = this.GetComponent<SpriteRenderer>();

        GameManager gm = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();

        sr.sprite = gm.maskSprites[playerNr - 1];
	}
}
