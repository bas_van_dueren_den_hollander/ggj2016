﻿using UnityEngine;
using System.Collections;

public class Hazard : MonoBehaviour {

	void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Player1" || other.tag == "Player2")
        {
            other.gameObject.SendMessage("TakeDamage", 5, SendMessageOptions.DontRequireReceiver);
        }
    }
}
