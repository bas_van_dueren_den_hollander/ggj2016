﻿using UnityEngine;
using System.Collections;

public class Chasing : MonoBehaviour {

    public GameObject target;

    public float maxSpeed = 5;
    public float maxSteering = 5;
    public bool evasion = false;

    void FixedUpdate()
    {
        if (target != null)
        {
            Vector2 desiredVelocity;
            Vector2 targetPositionPrediction = new Vector2(target.transform.position.x, target.transform.position.y) + target.GetComponent<Rigidbody2D>().velocity;

            desiredVelocity = targetPositionPrediction - new Vector2(GetComponent<Rigidbody2D>().transform.position.x, GetComponent<Rigidbody2D>().transform.position.y);
           

            Vector2 steering = desiredVelocity - GetComponent<Rigidbody2D>().velocity;

            steering = Vector2.ClampMagnitude(steering, maxSteering) / GetComponent<Rigidbody2D>().mass;

            GetComponent<Rigidbody2D>().velocity = Vector3.ClampMagnitude(GetComponent<Rigidbody2D>().velocity + steering, maxSpeed);

        }

    }
}
