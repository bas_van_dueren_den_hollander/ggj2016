﻿using UnityEngine;
using System.Collections;

public class SnakeTail : MonoBehaviour {

    public float duration = 0.3f;

	// Use this for initialization
	void Start () {
		StartCoroutine("Lifespan",duration);
	}

	IEnumerator Lifespan(float lifespan){
		yield return new WaitForSeconds(lifespan);
		Destroy(this.gameObject);
	}
}
