﻿ using UnityEngine;
using System.Collections;
//using UnityEditor;
using UnityEngine.UI;

public class TotemPickup : MonoBehaviour {

	private EquippedTotem totemEquipped;

    private Image totemHead;
    private Image totemBody;
    private Image totemLegs;
    public Sprite[] SkillImages;
    public Image HeadImageP1;
    public Image BodyImageP1;
    public Image LegImageP1;
    public Image HeadImageP2;
    public Image BodyImageP2;
    public Image LegImageP2;

	public GameObject playerManager;
	public GameObject equippedFeet;
	public GameObject equippedHead;
	public GameObject equippedBody;
	private TotemList totemList;

    private bool onTotem = false;
	public GameObject totemBeneathPlayer;

    private int controlScheme = 1;

	void Awake(){
       
        SkillImages = Resources.LoadAll<Sprite>("Sprites");
        HeadImageP1 = GameObject.FindGameObjectWithTag("P1Head").GetComponent<Image>();
        BodyImageP1 = GameObject.FindGameObjectWithTag("P1Body").GetComponent<Image>();
        LegImageP1 = GameObject.FindGameObjectWithTag("P1Legs").GetComponent<Image>();
        HeadImageP2 = GameObject.FindGameObjectWithTag("P2Head").GetComponent<Image>();
        BodyImageP2 = GameObject.FindGameObjectWithTag("P2Body").GetComponent<Image>();
        LegImageP2 = GameObject.FindGameObjectWithTag("P2Legs").GetComponent<Image>();

/*      if (this.gameObject.tag == "Player1"){
			totemEquipped = AssetDatabase.LoadAssetAtPath<EquippedTotem>("Assets/Resources/Scriptable Objects/EquippedTotemsP1.asset");
           
          
		} else {
			totemEquipped = AssetDatabase.LoadAssetAtPath<EquippedTotem>("Assets/Resources/Scriptable Objects/EquippedTotemsP2.asset");
           
        }
        Debug.Log(totemEquipped);*/
    }

    void Start()
    {
        controlScheme = this.GetComponent<PlayerMovement>().controlScheme;
		totemList = playerManager.GetComponent<TotemList>();
		EquippedObjectList temp = this.gameObject.GetComponent<EquippedObjectList>();
		equippedFeet = temp.equippedFeet;
		equippedBody = temp.equippedBody;
		equippedHead = temp.equippedHead;
    }

	void Update(){
        string useButton = "Use_P" + controlScheme;

        if (onTotem && Input.GetButtonDown(useButton)){
			string checkType = totemBeneathPlayer.gameObject.name;
//			TotemItemList list = AssetDatabase.LoadAssetAtPath<TotemItemList> ("Assets/Resources/Scriptable Objects/TotemList.asset");
            Debug.Log("Use key");

			if (totemBeneathPlayer.gameObject.name.Contains("Head") && this.gameObject.tag == ("Player1")){
				for (int i = 0; i < totemList.heads.Length; i++)
                {
					if (totemBeneathPlayer.gameObject.name.Contains(totemList.heads[i].name))
                    {
						equippedHead = totemList.heads[i];
						PlayerMagic temp = this.gameObject.GetComponent<PlayerMagic>();
						temp.equippedHead = equippedHead;
                    }
                }

                for ( int x =0; x < SkillImages.Length; x++)
                    {

                    if (totemBeneathPlayer.gameObject.name.Contains(SkillImages[x].name))
                    {
                        HeadImageP1.sprite = SkillImages[x];
                    }
                    }
				
			}
            if (totemBeneathPlayer.gameObject.name.Contains("Head") && this.gameObject.tag == ("Player2"))
            {
				for (int i = 0; i < totemList.heads.Length; i++)
                {
					if (totemBeneathPlayer.gameObject.name.Contains( totemList.heads[i].name))
                    {
						equippedHead = totemList.heads[i];
						PlayerMagic temp = this.gameObject.GetComponent<PlayerMagic>();
						temp.equippedHead = equippedHead;
                        HeadImageP2.sprite = SkillImages[i];
                    }
                
                }
                for (int x = 0; x < SkillImages.Length; x++)
                {
                    if (totemBeneathPlayer.gameObject.name.Contains(SkillImages[x].name))
                    {
                        HeadImageP2.sprite = SkillImages[x];
                    }
                }
            }
            
            if (totemBeneathPlayer.gameObject.name.Contains("Body") && this.gameObject.tag == ("Player1")){
				for (int i = 0; i < totemList.bodies.Length; i++)
                {
                    Debug.Log(i);
					if (totemBeneathPlayer.gameObject.name.Contains( totemList.bodies[i].name)){
						equippedBody= totemList.bodies[i];
						PlayerDagger temp = this.gameObject.GetComponent<PlayerDagger>();
						temp.equippedBody = equippedBody;
                
                    }
                    for (int x = 0; x < SkillImages.Length; x++)
                    {
                        if (totemBeneathPlayer.gameObject.name.Contains(SkillImages[x].name))
                        {
                            BodyImageP1.sprite = SkillImages[x];
                        }
                    }
                }
			}
            if (totemBeneathPlayer.gameObject.name.Contains("Body") && this.gameObject.tag == ("Player2")){
				for (int i = 0; i < totemList.bodies.Length; i++)
                {
					if (totemBeneathPlayer.gameObject.name.Contains(totemList.bodies[i].name))
                    {
						equippedBody= totemList.bodies[i];
						PlayerDagger temp = this.gameObject.GetComponent<PlayerDagger>();
						temp.equippedBody = equippedBody;

                    }
                    for (int x = 0; x < SkillImages.Length; x++)
                    {
                        if (totemBeneathPlayer.gameObject.name.Contains(SkillImages[x].name))
                        {
                            BodyImageP2.sprite = SkillImages[x];
                        }
                    }
                }
            }
            
            if (totemBeneathPlayer.gameObject.name.Contains("Feet") && this.gameObject.tag == ("Player1")){
				for (int i = 0; i < totemList.feet.Length; i++) {
					if(totemBeneathPlayer.gameObject.name.Contains(totemList.feet[i].name)){
						equippedFeet = totemList.feet[i];
						PlayerMovement temp = this.gameObject.GetComponent<PlayerMovement>();
						Health temp2 = this.gameObject.GetComponent<Health>();
						temp.equippedFeet = equippedFeet;
						temp2.equippedFeet = equippedFeet;
                        LegImageP1.sprite = SkillImages[i];
                    }
				}
                for (int x = 0; x < SkillImages.Length; x++)
                {
                    if (totemBeneathPlayer.gameObject.name.Contains(SkillImages[x].name))
                    {
                        LegImageP1.sprite = SkillImages[x];
                    }
                }
            }
            if (totemBeneathPlayer.gameObject.name.Contains("Feet") && this.gameObject.tag == ("Player2"))
            {
				for (int i = 0; i < totemList.feet.Length; i++)
                {
					if (totemBeneathPlayer.gameObject.name.Contains(totemList.feet[i].name))
                    {
						equippedFeet = totemList.feet[i];
                        LegImageP2.sprite = SkillImages[i];
                    }
                }
                for (int x = 0; x < SkillImages.Length; x++)
                {
                    if (totemBeneathPlayer.gameObject.name.Contains(SkillImages[x].name))
                    {
                        LegImageP2.sprite = SkillImages[x];
                    }
                }
            }
            Destroy(totemBeneathPlayer);
			onTotem = false;
		}
	}

	void OnTriggerEnter2D(Collider2D other){
		if(other.gameObject.tag == "TotemPart")
        {
            onTotem = true;
			totemBeneathPlayer = other.gameObject;
		}
	}

	void OnTriggerExit2D(Collider2D other){
		if(other.gameObject.tag == "TotemPart")
        {
			onTotem = false;
		}
	}
}
