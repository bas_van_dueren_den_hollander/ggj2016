﻿using UnityEngine;
using System.Collections;

public class Depth : MonoBehaviour {

	void Awake()
    {
        GetComponent<Renderer>().sortingOrder = -(int)this.transform.position.y;
    }
}
