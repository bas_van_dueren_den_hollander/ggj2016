﻿using UnityEngine;
using System.Collections;

public class RestartLevel : MonoBehaviour {

	void Restart()
    {
        Application.LoadLevel(0);
    }

    void StartGame()
    {
        Application.LoadLevel(Application.loadedLevel + 1);
    }

    public void QuitGame()
    {
        Application.Quit();
    }
     public void CreditsScreen()
    {
        Application.LoadLevel(4);
    }
}
