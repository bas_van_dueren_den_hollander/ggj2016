﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GameManager : MonoBehaviour {

    public string playerWin;
    public Sprite[] maskSprites = new Sprite[2];
    private int counter = 0;

    private int winningMask;

	public void GameOver()
    {
        GameObject p1Health = GameObject.Find("Player_1");
        GameObject p2Health = GameObject.Find("Player_2");

        Debug.Log(p1Health);

        if (p1Health.GetComponent<Health>().hitPoints > p2Health.GetComponent<Health>().hitPoints)
        {
            playerWin = "Player 1";
            winningMask = 0;
        } else if (p1Health.GetComponent<Health>().hitPoints == p2Health.GetComponent<Health>().hitPoints)
        {
            playerWin = "Nobody";
            winningMask = -1;
        } else
        {
            winningMask = 1;
            playerWin = "Player 2";
        }


        //GameObject.FindGameObjectWithTag("PlayerWon").GetComponent<Text>().text = playerWin;
   
        StartCoroutine(Loader());
}


    public void GetMask(Sprite newMask)
    {
        if(counter == 0)
        {
            maskSprites[0] = newMask;
            counter++;
        }
        else if (counter == 1)
        {
            maskSprites[1] = newMask;
            counter = 0;
            Application.LoadLevel(Application.loadedLevel + 1);
        }
    
    }

    void PutMask()
    {

    }
protected IEnumerator Loader()
    {
        Application.LoadLevel(Application.loadedLevel + 1);
        yield return null;
        GameObject.FindGameObjectWithTag("PlayerWon").GetComponent<Text>().text = playerWin;

        Debug.Log(winningMask);

        if(winningMask >= 0)
        {
            GameObject.FindGameObjectWithTag("WinningMask").GetComponent<Image>().sprite = maskSprites[winningMask];
        }
    }
}
