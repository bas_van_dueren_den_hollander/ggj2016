﻿using UnityEngine;
using System.Collections;

public class CameraViewScript : MonoBehaviour {


    public GameObject Player1;
    public GameObject Player2;
    private float averageYPosition;
    private float averageXPosition;
    private Vector3 NewCameraPosition;
   
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

        averageYPosition = ((Player1.transform.position.y + Player2.transform.position.y) / 2);
        averageXPosition = ((Player1.transform.position.x+ Player2.transform.position.x) / 2);
        NewCameraPosition =  new Vector3 (averageXPosition,averageYPosition,-10.0f);
        this.transform.position = NewCameraPosition;
    
     
}
}
