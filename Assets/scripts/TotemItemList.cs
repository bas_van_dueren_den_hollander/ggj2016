﻿using UnityEngine;
using System.Collections;

public class TotemItemList : ScriptableObject {
	public GameObject[] heads = null;
	public GameObject[] bodies = null;
	public GameObject[] feet = null;
}
