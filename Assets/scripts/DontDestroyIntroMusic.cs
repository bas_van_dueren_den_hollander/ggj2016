﻿using UnityEngine;
using System.Collections;

public class DontDestroyIntroMusic : MonoBehaviour {
   
	// Use this for initialization
	void Awake () {
     
        DontDestroyOnLoad(this.gameObject);

        if (FindObjectsOfType(GetType()).Length > 1)
        {
            Destroy(gameObject);
        }
    }
	
	// Update is called once per frame
	void Update () {
        if(Application.loadedLevel == 2)
        {
            Destroy(gameObject);
        }

    }
}
